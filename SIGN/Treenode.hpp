#pragma once

#include <string>

class treeNode
{
	public:
		treeNode(int inSize);
		treeNode(int* inArr, int inSize, std::string inString);
		~treeNode();
		treeNode* Evaluate();
		int IsTerminal();
		std::string GetString();
		treeNode* Next();
		void SetNext(treeNode* newNext);
	private:
		int* ArrayOfLockedBits;
		int sizeOfArr;
		std::string CurrString;
		treeNode* next;
};
