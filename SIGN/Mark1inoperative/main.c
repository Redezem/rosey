#include <stdio.h>
#include <stdlib.h>

#include "treestruct.h"

struct treebranch* ConstructArray(int inSize, struct treestruct* prevArr);
void PermuteOneLevel(struct treebranch*, char* currentStr);

int main(int argc, char** argv)
{
	if(argc < 1)
	{
		printf("Hey, there's not enough args here\nI'ma kill myself\n");
		return 1;
	}
	
	char* InitString = (char*)malloc(sizeof(char)*18);
	
	for(int i=0; i<18; i++)
	{
		InitString[i] = ' ';
	}
	
	struct treebranch *zeroNode;
	
	printf("%d\n",atoi(argv[1]));
	
	printf("now building array...\n");
	
	zeroNode=ConstructArray(atoi(argv[1]), NULL);
	
	printf("Done!\nNow permuting...\n");
	
	
	
	PermuteOneLevel(zeroNode,InitString);
	
	printf("Also done!\n");
	
	return 0;
}

struct treebranch* ConstructArray(int inSize, struct treestruct* prevArray)
{
	struct treebranch *zeroNode,*cursor,*cursorNew;
	struct treestruct *temp, *tprevArray;
	int posExists, negExists;
	posExists = 0;
	negExists = 0;
	zeroNode=NULL;
	cursor=NULL;
	cursorNew=NULL;
	temp=NULL;
	
	//printf("Now constructing string tree\n");
	printf("dong");
	for(int i=0;i<inSize;i++)
	{
		posExists = 0;
		negExists = 0;
		printf("ding");
		//printf("Checking for %d\n",i+1);
		
		if(prevArray != NULL)
		{
			tprevArray = prevArray;
			while(tprevArray!=NULL)
			{
				//printf("blip\n");
				if(tprevArray->val == i+1 && tprevArray->negFlag==0)
				{
					posExists=1;
					negExists=1; //OH NOES D: BAD CODING PRACTICE! -- Rectified
					//Well fuck this isn't good, I need to be able to do it for *all* things in the tree above. AH FUCK IT FINE I'LL PUT IN A REVERSE LINK IN THE DATA STRUCT -- Also rectified
				}else if(tprevArray->val == i+1 && tprevArray->negFlag==1)
				{
					negExists=1; //Have made change to program range, this is the nice way of doing it.
					posExists=1;
				}
				//printf("Going back one \n");
				tprevArray=tprevArray->prev;
				
			}
		}
		//At this point, everything is good :)
		if(posExists==0)
		{
			temp = (struct treestuct*)malloc(sizeof(struct treestruct));
			if(temp == NULL)
			{
				printf("No ram\n");
				exit(1);
			}
			temp->val=i+1;
			temp->negFlag=0;
			temp->prev = prevArray;
			temp->targetlist = NULL;
			cursorNew = (struct treebranch*)malloc(sizeof(struct treebranch));
			if(cursorNew == NULL)
			{
				printf("No ram\n");
				exit(1);
			}
			cursorNew->target = temp;
			if(zeroNode == NULL)
			{
				zeroNode=cursorNew;
				cursor = cursorNew;
			}else{
				cursor->next=cursorNew;
				cursor = cursorNew;
			}
		}
		if(negExists==0)
		{
			temp = (struct treestuct*)malloc(sizeof(struct treestruct));
			if(temp == NULL)
			{
				printf("No ram\n");
				exit(1);
			}
			temp->val=i+1;
			temp->negFlag=1;
			temp->prev = prevArray;
			temp->targetlist=NULL;
			cursorNew = (struct treebranch*)malloc(sizeof(struct treebranch));
			if(cursorNew == NULL)
			{
				printf("No ram\n");
				exit(1);
			}
			cursorNew->target = temp;
			if(zeroNode == NULL)
			{
				zeroNode=cursorNew;
				cursor = cursorNew;
			}else{
				cursor->next=cursorNew;
				cursor = cursorNew;
			}
		}

	}
	
	cursor = zeroNode;
	while(cursor != NULL)
	{
		cursor->target->targetlist=ConstructArray(inSize,cursor->target);
		cursor=cursor->next;
	}
	return zeroNode;
}

void PermuteOneLevel(struct treebranch* zNode, char* newStr)
{
	char* curString;
	struct treebranch* cursor;
	int blankIndex = -1;
	
	printf("go!\n");
	
	curString = (char*)malloc(sizeof(char)*18);
	for(int i=0; i<18; i++)
	{
		curString[i]=newStr[i];
	}
	//logcate next blank cell and insert target's letter
	
	for(int i=0; i<18; i++)
	{
		if(curString[i]== ' ')
		{
			blankIndex = i;
			printf("Blank!\n");
			break;
		}
	}
	
	if(blankIndex == -1)
	{
		printf("An error occurred: String is somehow full o.o\n");
		return;
	}
	
	if(zNode->target->negFlag==1)
	{
		curString[blankIndex]='-';
	}
	else
	{
		curString[blankIndex]='+';
	}
	curString[blankIndex+1]=(char)(((int)'0')+zNode->target->val);
	curString[blankIndex+2]=',';
	
	if(zNode->target->targetlist!=NULL)
	{
		//stuff to do with recursoion here
		cursor = zNode->target->targetlist;
		while(cursor != NULL)
		{
			PermuteOneLevel(cursor,curString);
			cursor=cursor->next;
		}
	}
	else
	{
		//write string out to filed
		FILE* f;
		f=fopen("output.txt","a");
		for(int i=0; i<18; i++)
		{
			fputc(curString[i],f);
		}
		fputc('\n',f);
	}
	
	return;
}
