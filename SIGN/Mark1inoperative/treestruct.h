#define TRUE 1
#define FALSE 0

struct treebranch
{
	struct treestruct* target;
	struct treebranch* next;
};

struct treestruct
{
	int val;
	int negFlag;
	struct treebranch* targetlist;
	struct treestruct* prev;
};
