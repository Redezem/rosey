#include "RNA2Prot.hpp"

RNA2Prot::RNA2Prot()
{
	dictionary["UUU"] = 'F';      
	dictionary["UUC"] = 'F';      
	dictionary["UUA"] = 'L';      
	dictionary["UUG"] = 'L';     
	dictionary["UCU"] = 'S';      
	dictionary["UCC"] = 'S';      
	dictionary["UCA"] = 'S';     
	dictionary["UCG"] = 'S';      
	dictionary["CCG"] = 'P';      
	dictionary["ACG"] = 'T';      
	dictionary["GCG"] = 'A';
	dictionary["UAU"] = 'Y';      
	dictionary["CAU"] = 'H';      
	dictionary["AAU"] = 'N';      
	dictionary["GAU"] = 'D';
	dictionary["UAC"] = 'Y';      
	dictionary["CAC"] = 'H';      
	dictionary["AAC"] = 'N';      
	dictionary["GAC"] = 'D';
	dictionary["CAA"] = 'Q';      
	dictionary["AAA"] = 'K';      
	dictionary["GAA"] = 'E';
	dictionary["CAG"] = 'Q';      
	dictionary["AAG"] = 'K';      
	dictionary["GAG"] = 'E';
	dictionary["UGU"] = 'C';      
	dictionary["CGU"] = 'R';      
	dictionary["AGU"] = 'S';      
	dictionary["GGU"] = 'G';
	dictionary["UGC"] = 'C';      
	dictionary["CGC"] = 'R';      
	dictionary["AGC"] = 'S';      
	dictionary["GGC"] = 'G';
	dictionary["CGA"] = 'R';      
	dictionary["AGA"] = 'R';      
	dictionary["GGA"] = 'G';
	dictionary["UGG"] = 'W';      
	dictionary["CGG"] = 'R';      
	dictionary["AGG"] = 'R';      
	dictionary["GGG"] = 'G';
	dictionary["CUU"] = 'L';      
	dictionary["AUU"] = 'I';      
	dictionary["GUU"] = 'V';
	dictionary["CUC"] = 'L';      
	dictionary["AUC"] = 'I';      
	dictionary["GUC"] = 'V';
	dictionary["CUA"] = 'L';            
	dictionary["GUA"] = 'V';
	dictionary["AUA"] = 'I';
	dictionary["CUG"] = 'L';      
	dictionary["AUG"] = 'M';      
	dictionary["GUG"] = 'V';
	dictionary["CCU"] = 'P';      
	dictionary["ACU"] = 'T';      
	dictionary["GCU"] = 'A';
	dictionary["CCC"] = 'P';      
	dictionary["ACC"] = 'T';      
	dictionary["GCC"] = 'A';
	dictionary["CCA"] = 'P';      
	dictionary["ACA"] = 'T';      
	dictionary["GCA"] = 'A';
}

char RNA2Prot::Lookup(std::string inStr)
{
	char returnChar;
	std::map<std::string,char>::iterator result;

	result = dictionary.find(inStr);

	if(result == dictionary.end())
	{
		return '%';
	}
	return result->second;
}