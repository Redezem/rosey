#include <map>
#include <string>

class RNA2Prot
{
public:
	RNA2Prot();
	char Lookup(std::string);
private:
	std::map<std::string,char> dictionary;
};